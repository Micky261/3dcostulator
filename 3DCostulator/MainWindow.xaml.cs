﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _3DCostulator {
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void menuMain_SettingsConfigure_Click(object sender, RoutedEventArgs e) {

        }

        private void menuMain_WindowReset_Click(object sender, RoutedEventArgs e) {

        }

        private void menuMain_WindowClose_Click(object sender, RoutedEventArgs e) {

        }

        private void menuMain_OpenHelp_Click(object sender, RoutedEventArgs e) {

        }

        private void textboxMaterial_Weight_TextChanged(object sender, TextChangedEventArgs e) {
            if (textboxMaterial_Weight.Text != "") {
                textboxMaterial_Length.Text = "";
                textboxMaterial_Length.IsEnabled = false;
            }
            else {
                textboxMaterial_Length.IsEnabled = true;
            }
        }

        private void textboxMaterial_Length_TextChanged(object sender, TextChangedEventArgs e) {
            if (textboxMaterial_Length.Text != "") {
                textboxMaterial_Weight.Text = "";
                textboxMaterial_Weight.IsEnabled = false;
            } else {
                textboxMaterial_Weight.IsEnabled = true;
            }
        }

        private void textboxPrints_Volume_TextChanged(object sender, TextChangedEventArgs e) {
            if (textboxPrints_Volume.Text != "") {
                textboxPrints_Length.Text = "";
                textboxPrints_Weight.Text = "";
                textboxPrints_Length.IsEnabled = false;
                textboxPrints_Weight.IsEnabled = false;
            }
            else {
                textboxPrints_Length.IsEnabled = true;
                textboxPrints_Weight.IsEnabled = true;
            }
        }

        private void textboxPrints_Length_TextChanged(object sender, TextChangedEventArgs e) {
            if (textboxPrints_Length.Text != "") {
                textboxPrints_Volume.Text = "";
                textboxPrints_Weight.Text = "";
                textboxPrints_Volume.IsEnabled = false;
                textboxPrints_Weight.IsEnabled = false;
            }
            else {
                textboxPrints_Volume.IsEnabled = true;
                textboxPrints_Weight.IsEnabled = true;
            }
        }

        private void textboxPrints_Weight_TextChanged(object sender, TextChangedEventArgs e) {
            if (textboxPrints_Weight.Text != "") {
                textboxPrints_Volume.Text = "";
                textboxPrints_Length.Text = "";
                textboxPrints_Volume.IsEnabled = false;
                textboxPrints_Length.IsEnabled = false;
            }
            else {
                textboxPrints_Volume.IsEnabled = true;
                textboxPrints_Length.IsEnabled = true;
            }
        }

        private void buttonStartCalculation_Click(object sender, RoutedEventArgs e) {

        }
    }
}
